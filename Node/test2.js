const Crawler = require("simplecrawler");
const crawler = new Crawler("http://docs.apigee.com");
const APIResponseType = ['application/json', 'application/xml']

function handleProbableAPI(url, responseBuffer, type) {

    /*let api = {}
    if (type === 'application/json')
        api = {
            url: url,
            response: responseBuffer.toJSON()
        }
    else
        api = {
            url: url,
            response: responseBuffer.toString('utf8')
        }*/
    let api = {
        url: url,
        response: responseBuffer.toString('utf8'),
        type: type
    }
    console.log(api)
}
crawler.maxDepth = 2;
/*crawler.userAgent = "nsc"*/
/*crawler.useProxy = true;
crawler.proxyHostname = "proxy.tcs.com"
    //crawler.address = "http://proxy.tcs.com"
crawler.proxyPort = 8080
crawler.ignoreInvalidSSL = true
crawler.proxyUser = '1132775'
crawler.proxyPass = "tataApr@2017"*/
crawler.parseHTMLComments = true
crawler.allowInitialDomainChange = true
crawler.on("fetchcomplete", function(queueItem, responseBuffer, response) {
    /* console.log("I just received %s (%d bytes)", queueItem.url, responseBuffer.length);
     console.log("It was a resource of type %s", response.headers['content-type']);*/

    if (APIResponseType.indexOf(response.headers['content-type']) >= 0) {
        handleProbableAPI(queueItem.url, responseBuffer, response.headers['content-type'])
    }
    /*console.log(responseBuffer.toString('utf8'))*/
}).on("crawlstart", function() { console.log('In') }).on("fetchclienterror", function(qi, err) {
    console.log(qi, err)
});
/*console.log(crawler)*/

crawler.start();