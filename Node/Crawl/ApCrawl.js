const config = require("./config.json")
const argv = require('minimist')(process.argv.slice(2));
const Crawler = require("simplecrawler");
const crawler = new Crawler(argv.seed);
const url = require('url')
const fs = require('fs')
const seed = url.parse(argv.seed)
console.log(seed)
const saveDir = argv.dir || config.rootDir + '/' + seed.hostname
if (!fs.existsSync(saveDir)) {
    fs.mkdirSync(saveDir);
} else {

}

crawler.maxDepth = argv.depth || config.depth;
crawler.on("fetchcomplete", function(queueItem, responseBuffer, response) {
    let saveFileName = url.parse(queueItem.url).pathname.replace(/\//g, '-')
    console.log(queueItem.url)
    console.log(response.headers['content-type'])
    if (/text\/html*/i.test(response.headers['content-type'])) {
        fs.writeFile(saveDir + '/' + saveFileName + '.html', responseBuffer)
    }
}).on("crawlstart", function() { console.log('In') }).on("fetchclienterror", function(qi, err) {
    console.log(qi, err)
});
crawler.start();